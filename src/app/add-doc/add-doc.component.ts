import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ElasticsearchService } from '../services/elasticsearch.service';

@Component({
  selector: 'app-add-doc',
  templateUrl: './add-doc.component.html',
  styleUrls: ['./add-doc.component.css']
})
export class AddDocComponent implements OnInit {
  
  isConnected = false;
  status: string;

  model: any = {
    title: '',
    province: '',
    detail: '',
    type: ''
  }

  rForm: FormGroup;

  title:string = '';
  province:string = '';
  detail: string = '';
  type: string = '';

  constructor(private fb: FormBuilder, private es: ElasticsearchService, private cd: ChangeDetectorRef) { 
    this.rForm = fb.group({
      'title': [null, Validators.required],
      'province': [null, Validators.required],
      'detail': [null, Validators.required],
      'type': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.es.isAvailable().then(() => {
      this.status = 'OK';
      this.isConnected = true;
    }, error => {
      this.status = 'ERROR';
      this.isConnected = false;
      console.error('ElasticServer is down', error);
    }).then(() => {
      this.cd.detectChanges();
    });
  }

  onSubmit(value) {

    this.es.addToIndex({
      index: 'review',
      type: value.type,
      body: {
        title: value.title,
        province: value.province,
        detail: value.detail
      }
    }).then((result) => {
      console.log(result);
      this.rForm.reset();
    }, error => {
      console.error(error);
    });
  }

}
