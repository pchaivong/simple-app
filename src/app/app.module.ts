import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ElasticsearchService } from './services/elasticsearch.service';


import { AppComponent } from './app.component';
import { AddDocComponent } from './add-doc/add-doc.component';


@NgModule({
  declarations: [
    AppComponent,
    AddDocComponent
  ],
  imports: [
    BrowserModule, NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ElasticsearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
